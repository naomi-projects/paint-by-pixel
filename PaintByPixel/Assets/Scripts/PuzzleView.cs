﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class PuzzleView : View
{
    /// <summary>
    /// Reference to the Game Manager.
    /// </summary>
    private GameManager gameManager;

    /// <summary>
    /// Reference to the currently selected/loaded puzzle info.
    /// </summary>
    public IndexedPuzzle CurrentPuzzle
    {
        get => currentPuzzle;
        set
        {
            currentPuzzle = value;
            ResetPuzzleProgress();
        }
    }
    private IndexedPuzzle currentPuzzle;

    //
    public bool Active { get; set; }

    // Loading 
    public bool IsLoaded { get; private set; }
    private bool loading;
    public event Action DoneLoading;

    // Color palette
    private int selectedColor;
    public int SelectedColor
    {
        get { return selectedColor; }
        private set
        {
            selectedColor = value;
            SelectedColorValue = CurrentPuzzle.Puzzle.colors[SelectedColor];
        }
    }
    public Color SelectedColorValue { get; private set; }

    // Coloring mode
    public bool EraseModeOn { get; private set; }
    public bool Erasing { get; set; }
    public bool Coloring { get; set; }

    // Puzzle pixel size values    
    public const int PIXEL_SIZE = 10;   // Pixel side length in world space units
    private Vector2 boxSize;            // Pixel width and height vector

    // UI elements
    [Header("UI elements: General")]
    public Canvas mainCanvas;
    [Header("UI elements: Menu Panel")]
    public ColorSwatch[] colorSwatches;
    public OnOffButton eraseButton;
    public MyButton undoButton, redoButton, zoom1Button, zoom2Button, zoom3Button;
    [Header("UI elements: Puzzle Panel")]
    public RectTransform puzzlePanel;
    public RectTransform puzzleBase;

    // Prefabs and Resources
    [Header("Prefabs & Resources")]
    [Tooltip("32 images, 1 for each color id, indexed by color id")]
    public Sprite[] basePixelImages;
    [Tooltip("Prefab to instantiate for each row of the puzzle. Should have an Image component.")]
    public Image rowPrefab;
    [Tooltip("Prefab to instantiate for each colored pixel of the puzzle. Should have an Image component.")]
    public Image colorOverlayPrefab;
    [Tooltip("Sprite for the colored pixel texture.")]
    public Sprite colorOverlaySprite;
    [Tooltip("Sprite for the incorrectly colored pixel icon.")]
    public Sprite warningIcon;

    // UI measurement variables
    private float puzzlePanelWidth, puzzlePanelHeight;
    private float puzzlePanelScale;
    private float canvasWidth, canvasHeight;
    private float screenRatio;
    private void SetDisplayMeasurements()
    {
        Rect r1 = mainCanvas.gameObject.GetComponent<RectTransform>().rect;
        canvasWidth = r1.width;
        canvasHeight = r1.height;
        puzzlePanelScale = puzzleBase.localScale.x;
        puzzlePanelWidth = puzzlePanel.rect.width / puzzlePanelScale;
        puzzlePanelHeight = puzzlePanel.rect.height / puzzlePanelScale;
        screenRatio = (Screen.width / (float)Screen.height);
        Debug.Log($"Screen dimensions: {Screen.width}x{Screen.height}");
    }

    // Current coloring progress
    private bool[] correctColors;
    private int[] currentColors;

    // Indexed references to the row objects and pixel objects
    private RectTransform[] pixels;
    private RectTransform[] rows;

    private void ResetPuzzleProgress()
    {
        correctColors = new bool[CurrentPuzzle.Puzzle.colorIds.Length];
        currentColors = new int[CurrentPuzzle.Puzzle.colorIds.Length];
        for (int i = 0; i < currentColors.Length; i++)
            currentColors[i] = -1;
    }
    
    private void Start()
    {
        gameManager = GameManager.Instance;
        if (!gameManager) Debug.LogError("GameManager does not exist in the scene.");

        SetDisplayMeasurements();

        boxSize = new Vector2(PIXEL_SIZE, PIXEL_SIZE);

        // assume index of most recent will always be an actual puzzle
        int index = gameManager.CurrentSave.IndexOfMostRecent;
        CurrentPuzzle = new IndexedPuzzle(gameManager.CurrentSave.GetPuzzle(index), index);
        SelectedColor = 0;

        eraseButton.Toggle += (bool on) =>
        {
            EraseModeOn = on;
            foreach (ColorSwatch item in colorSwatches)
            {
                item.Select(false);
            }
            if (!on)
                colorSwatches[SelectedColor].Select(true);

        };

        IsLoaded = false;
        loading = false;

        StartSettingUp();
    }

    public void StartSettingUp()
    {
        loading = true;
        Reload();
    }

    /// <summary>
    /// Call this when entering the Puzzle screen.
    /// </summary>
    /// <param name="index">Integer index of the puzzle in the current save's list.</param>
    public override void Activate(bool active)
    {
        mainCanvas.enabled = active;
        if (!active) return;

        int index = gameManager.CurrentSave.IndexOfMostRecent;

        if (index < 0 || index >= gameManager.CurrentSave.Puzzles.Count)
        {
            Debug.Log("Index of puzzle out of bounds.");
            return;
        }

        Active = true;

        // this should never be true
        if (CurrentPuzzle == null)
        {
            // set the current puzzle and reload the puzzle view
            CurrentPuzzle = new IndexedPuzzle(gameManager.CurrentSave.GetPuzzle(index), index);
        }
        else if (CurrentPuzzle.IndexInSaveData == index)
        {
            // On starting the app and pressing "Start", the program will go here.
            // Don't need to reload if it is already loading.
            if (loading) return;
            // don't need to reload if the current puzzle is correct and already loaded
            if (IsLoaded) return;
        }
        else
        {
            // change the current puzzle to the correct puzzle and reload
            CurrentPuzzle = new IndexedPuzzle(gameManager.CurrentSave.GetPuzzle(index), index);
        }

        Debug.Log($"Puzzle view active: {CurrentPuzzle.Puzzle.Name}");

        Reload();
    }

    /// <summary>
    /// Resets the Puzzle screen incl. puzzle image and color swatch buttons
    /// </summary>
    private void Reload()
    {
        if (CurrentPuzzle == null || (!Active && !loading))
        {
            Debug.Log($"CurrentPuzzle == null: {CurrentPuzzle == null}");
            Debug.Log($"!Active && !loading: {!Active && !loading}");
            return;
        }
        Debug.Log($"Reloading...");
        foreach (ColorSwatch item in colorSwatches)
        {
            item.gameObject.SetActive(false);
        }

        for (int i = 0; i < CurrentPuzzle.Puzzle.colors.Length; i++)
        {
            colorSwatches[i].gameObject.SetActive(true);
            colorSwatches[i].myInt = i;
            colorSwatches[i].SetColor(CurrentPuzzle.Puzzle.colors[i]);

            colorSwatches[i].Selected += (int id) =>
            {
                SelectedColor = id;

                //DisplayDebug($"Selected color id: {SelectedColor}");
                for (int j = 0; j < colorSwatches.Length; j++)
                {
                    if (j != SelectedColor)
                        colorSwatches[j].Select(false);
                }
                if (EraseModeOn)
                    eraseButton.Click();
            };
        }

        colorSwatches[0].Select(true);

        MakeInteractablePuzzle();
    }

    /// <summary>
    /// Reset the puzzle with new pixels.
    /// </summary>
    private void MakeInteractablePuzzle()
    {
        ClearPuzzle();

        int width = CurrentPuzzle.Puzzle.wBoxes;
        int height = CurrentPuzzle.Puzzle.hBoxes;

        pixels = new RectTransform[width * height];
        rows = new RectTransform[height];
       
        Debug.Log($"Creating pixels: {width}x{height}");

        // set up the puzzle base
        puzzleBase.sizeDelta = new Vector2(PIXEL_SIZE * width, PIXEL_SIZE * height);
        puzzleBase.localPosition = Vector2.zero;

        IEnumerator makeSomePixels = MakeSomePixels(width, height);
        StartCoroutine(makeSomePixels);
    }

    /// <summary>
    /// Make some pixels.
    /// </summary>
    /// <param name="width">Number of columns of pixels.</param>
    /// <param name="height">Number of rows of pixels.</param>
    /// <returns></returns>
    IEnumerator MakeSomePixels(int width, int height)
    {
        while (CurrentPuzzle == null)
            yield return null;

        int wBoxes = CurrentPuzzle.Puzzle.wBoxes;

        for (int h = 0; h < height; h++)
        {
            Texture2D[] row = new Texture2D[width];
            // Make a row of pixels
            for (int w = 0; w < width; w++)
            {
                int index = w + (h * wBoxes);
                // get the correct base images associated with each color id
                row[w] = basePixelImages[CurrentPuzzle.Puzzle.colorIds[index]].texture;
            }
            // merge the row of images into 1 image
            Texture2D mergedRow = MergeHorizontal(row);
            // create the game object for the row
            GameObject rowObject = MakeRow(mergedRow, h);
            rows[h] = rowObject.GetComponent<RectTransform>();

            Debug.Log($"Line of pixels created.");
            yield return null;
        }
        Debug.Log($"Created some pixels: {width * height}");

        IsLoaded = true;
        loading = false;
        DoneLoading?.Invoke();
    }

    /// <summary>
    /// Remove all the pixels attached to the puzzle base.
    /// </summary>
    private void ClearPuzzle()
    {
        foreach (RectTransform item in puzzleBase.GetComponentsInChildren<RectTransform>())
        {
            if (item.CompareTag("row") || item.CompareTag("pixel") || item.CompareTag("colorOverlay") || item.CompareTag("warning"))
                GameObject.Destroy(item.gameObject);
        }
    }


    public void SetEraseModeOn(bool on)
    {
        EraseModeOn = on;
    }

    //! Zoom the puzzle: 
    //! 1 - zoom out to full screen (can't color in this mode)
    //! 2 - default zoom (1x) to work on puzzle
    //! 3 - 1.5x zoomed from default
    public void Zoom(int zoom = 1)
    {
        int actualZoom = Mathf.Clamp(zoom, 1, 3);
        Debug.Log($"Zoom: {actualZoom}");
    }

    /// <summary>
    /// Called in every Update call.
    /// </summary>
    protected override void UpdateFunction()
    {
        if (!Active) return;

        HandleInput();
    }

    /// <summary>
    /// Do this when exiting the PuzzleView
    /// </summary>
    public void Exit()
    {
        Active = false;
    }

    /// <summary>
    /// Makes a row of pixels. Putting individual textures into one composite texture.
    /// Assumes textures in sourceTextures are all the same size and not null.
    /// </summary>
    private Texture2D MergeHorizontal(Texture2D[] sourceTextures)
    {
        if (sourceTextures == null)
        {
            Debug.LogError("source textures array is null.");
            return null;
        }

        if (sourceTextures.Length <= 1)
        {
            Debug.LogError("source textures array is empty or 1.");
            return null;
        }

        int sourceWidth = sourceTextures[0].width;
        int sourceHeight = sourceTextures[0].height;

        Texture2D resultTexture = new Texture2D(sourceWidth * sourceTextures.Length, sourceHeight)
        {
            filterMode = FilterMode.Point
        };

        //Debug.Log($"Original: {sourceWidth}x{sourceHeight}");
        //Debug.Log($"{sourceTextures.Length} wide: {resultTexture.width}x{resultTexture.height}");

        // pixels are laid out left to right, bottom to top
        Color[] newColors = new Color[resultTexture.width * resultTexture.height];

        // for each image in source textures
        for (int k = 0; k < sourceTextures.Length; k++)
        {
            // get the array of pixel colors
            Color[] sourceColors = sourceTextures[k].GetPixels();
            for (int i = 0; i < sourceWidth; i++)
            {
                for (int j = 0; j < sourceHeight; j++)
                {
                    newColors[i + (sourceTextures.Length * sourceWidth * j) + (k * sourceWidth)] = sourceColors[i + (sourceWidth * j)];

                }
            }
            //Debug.Log($"Added an image to the row.");
        }

        resultTexture.SetPixels(newColors);
        resultTexture.Apply();

        Debug.Log("Merged a row using MergeHorizontal.");
        return resultTexture;
    }

    private GameObject MakeRow(Texture2D mergedRow, int row)
    {
        GameObject newRow = Instantiate(rowPrefab.gameObject, puzzleBase);
        newRow.transform.localPosition = new Vector3(0, (row * PIXEL_SIZE) - (puzzleBase.rect.height / 2) + (PIXEL_SIZE / 2), 0);

        Sprite sprite = Sprite.Create(mergedRow,
                new Rect(0.0f, 0.0f, mergedRow.width, mergedRow.height), // portion of the texture to use (all)
                new Vector2(0.5f, 0.5f));   // pivot point (center)
        newRow.GetComponent<Image>().sprite = sprite;
        newRow.GetComponent<RectTransform>().sizeDelta = new Vector2(mergedRow.width / 3.2f, mergedRow.height / 3.2f);

        newRow.tag = "row";
        return newRow;
    }

    private void HandleInput()
    {
        // PC: left mouse click
        // Android: single touch
        if (Input.GetKey(KeyCode.Mouse0))
        {
            // get the index of the clicked pixel based on the mouse position click on the row object
            Vector2 mousePosInWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // x
            float xOn1to1Scale = mousePosInWorld.x / (Camera.main.orthographicSize * screenRatio);
            float xOn0to1Scale = (xOn1to1Scale + 1) / 2;
            float xRelativeToCanvas = xOn0to1Scale * canvasWidth; // on 0 to + scale
            float xRelativeToPuzzlePanel = xRelativeToCanvas / puzzlePanelWidth / puzzlePanelScale; // on 0 to 1 scale

            if (xRelativeToPuzzlePanel > 1) return; // mouse click on menu panel

            float xRelativeToPuzzlePanelOn1to1 = (xRelativeToPuzzlePanel * 2) - 1; // on -1 to 1 scale
            float xRelativeToPuzzle = xRelativeToPuzzlePanelOn1to1 * (puzzlePanelWidth / 2);
            int column = ((int)xRelativeToPuzzle + (CurrentPuzzle.Puzzle.wBoxes * PIXEL_SIZE / 2)) / PIXEL_SIZE;
            Debug.Log($"column: {column}");

            // y
            float yOn1to1Scale = mousePosInWorld.y / (Camera.main.orthographicSize);
            float yOn0to1Scale = (yOn1to1Scale + 1) / 2;
            float yRelativeToCanvas = yOn0to1Scale * canvasHeight; // on 0 to + scale
            float yRelativeToPuzzlePanel = yRelativeToCanvas / puzzlePanelHeight / puzzlePanelScale; // on 0 to 1 scale

            float yRelativeToPuzzlePanelOn1to1 = (yRelativeToPuzzlePanel * 2) - 1; // on -1 to 1 scale
            float yRelativeToPuzzle = yRelativeToPuzzlePanelOn1to1 * (puzzlePanelHeight / 2);
            int row = ((int)yRelativeToPuzzle + (CurrentPuzzle.Puzzle.hBoxes * PIXEL_SIZE / 2)) / PIXEL_SIZE;
            Debug.Log($"row: {row}");

            int index = column + (CurrentPuzzle.Puzzle.wBoxes * row);
            Debug.Log($"index: {index}");

            // Erasing
            if (EraseModeOn)
            {
                if (currentColors[index] == -1)
                {
                    //Debug.Log($"{index} already erased: {currentColors[index]}");
                    return;
                }

                //Debug.Log($"Erasing index: {index}, current color: {currentColors[index]}");

                // set correct color to false, current color to -1
                correctColors[index] = false;
                currentColors[index] = -1;

                //result.gameObject.SetActive(false);
                // or this?
                Destroy(pixels[index].gameObject);

                //Debug.Log($"Done erasing index: {index}, current color: {currentColors[index]}");
            }
            // Coloring
            else
            {
                if (currentColors[index] != -1)
                {
                    //Debug.Log($"{index} already colored: {currentColors[index]}");
                    return;
                }
                currentColors[index] = 1; // temporarily set it

                //Debug.Log($"Coloring index: {index}, current color: {currentColors[index]}");

                // create the pixel object, set the size
                GameObject pixel = Instantiate(colorOverlayPrefab.gameObject, rows[row]);
                pixels[index] = pixel.GetComponent<RectTransform>();
                pixels[index].sizeDelta = new Vector2(PIXEL_SIZE, PIXEL_SIZE);

                // set position along the x-axis w/ respect to the row
                int xPos = (column * PIXEL_SIZE) + PIXEL_SIZE / 2 - CurrentPuzzle.Puzzle.wBoxes * PIXEL_SIZE / 2;
                pixels[index].localPosition = new Vector3(xPos, 0, 0);

                // set image to colorOverlay and color to puzzleView selected color
                Image img = pixel.GetComponent<Image>();
                img.sprite = colorOverlaySprite;
                img.color = SelectedColorValue;

                // set correct color to true/false, current color to puzzleView selected color
                if (SelectedColor != CurrentPuzzle.Puzzle.colorIds[index])
                {
                    correctColors[index] = false;

                    // create warning icon
                }
                else
                {
                    correctColors[index] = true;
                }
                currentColors[index] = SelectedColor;
                //Debug.Log($"Done coloring index: {index}, current color: {currentColors[index]}");
            }
        }
        // PC: right mouse click
        // Android: double touch
        else if (Input.GetKey(KeyCode.Mouse1))
        {
        }
    }
}
