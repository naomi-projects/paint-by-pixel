﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PuzzleCreationView : View
{
    /// <summary>
    /// Reference to the GameManager in the scene.
    /// </summary>
    private GameManager gameManager;

    /// <summary>
    /// Reference to the Puzzle Creation object in the scene
    /// </summary>
    private PuzzleCreation puzzleCreator;

    /// <summary>
    /// Sample image used for testing in the Unity Editor.
    /// </summary>
    public Texture2D testTexture;

    /// <summary>
    /// Reference to the Canvas component that displays the Puzzle Creation View.
    /// </summary>
    public Canvas mainCanvas;

    /// <summary>
    /// Menu displayed when the user saves a puzzle. Prompts the user
    /// to create another puzzle, play the created puzzle, or return to the library.
    /// </summary>
    public Canvas savePopup;

    /// <summary>
    /// Button on the Save Popup menu. Used to return to and refresh the Puzzle Creation View.
    /// </summary>
    public Button createAnother;

    // Open device storage to find an image, make the puzzle, save the puzzle
    public Button findImage;
    public Button makePuzzle; 
    public Button savePuzzle;
    public Image original, pixelated;
    public GameObject colorsPanel;
    public GameObject[] swatches;
    public Text dimensions;
    public GameObject tagsPanel;
    public GameObject[] tags;
    public InputField tagInput;
    public Button addTag;
    public GameObject savePanel;
    public InputField nameInput;

    // Temporary storage used for custom puzzle creation
    private Texture2D tempOriginalTexture, tempPuzzleTexture;
    private Sprite tempOriginalSprite, tempPuzzleSprite;

    /// <summary>
    /// True if the app is waiting for the user to give permission to access the device storage.
    /// </summary>
    private bool waitingForPermissionToGetImage = false;

    private void Start()
    {
        gameManager = GameManager.Instance;
        if (!gameManager) Debug.LogError("GameManager does not exist in the scene.");

        puzzleCreator = FindObjectOfType<PuzzleCreation>();
        if (puzzleCreator == null) Debug.LogError("PuzzleCreator does not exist in the scene.");
        // Do these things after the puzzle is created!
        puzzleCreator.PuzzleCreated += OnPuzzleCreated;

        savePopup.enabled = false;

        findImage.onClick.AddListener(GetImageFromGallery);
        makePuzzle.onClick.AddListener(CreatePuzzleFromImage);
        savePuzzle.onClick.AddListener(SavePuzzle);
        addTag.onClick.AddListener(AddTag);

        createAnother.onClick.AddListener(SaveCreationAndStay);

    }

    protected override void UpdateFunction()
    {
        // This section is used by GetImageFromGallery to wait for user permission to access device storage.
        if (waitingForPermissionToGetImage)
        {
            // User gave permission
            if (gameManager.PermissionReceived)
            {
                waitingForPermissionToGetImage = false;
                GetImageHelper();
            }
            else
            {
                return;
            }
        }

        if (nameInput.textComponent.text == null) savePuzzle.interactable = false;
        else if (nameInput.textComponent.text.CompareTo("") == 0) savePuzzle.interactable = false;
        else savePuzzle.interactable = true;

        // Puzzle Creation Screen: tag buttons
        bool maxTags = true;
        foreach (GameObject tag in tags)
        {
            if (!tag.activeInHierarchy)
            {
                maxTags = false;
                break;
            }
        }

        // Puzzle Creation Screen: adding tags 
        if (maxTags)
        {
            addTag.interactable = false;
            tagInput.text = "";
            tagInput.interactable = false;
        }
        else if (tagInput.textComponent.text == null)
        {
            addTag.interactable = false;
            tagInput.interactable = true;
        }
        else if (tagInput.textComponent.text.CompareTo("") == 0)
        {
            addTag.interactable = false;
            tagInput.interactable = true;
        }
        else
        {
            addTag.interactable = true;
            tagInput.interactable = true;
        }
    }

    public override void Activate(bool active)
    {
        mainCanvas.enabled = active;
        if (!active) return;

        findImage.gameObject.SetActive(true);
        findImage.interactable = true;
        makePuzzle.gameObject.SetActive(false);
        savePuzzle.gameObject.SetActive(false);
        colorsPanel.SetActive(false);
        original.gameObject.SetActive(false);
        pixelated.gameObject.SetActive(false);
        tagsPanel.SetActive(false);
        savePanel.SetActive(false);
        savePopup.enabled = false;
    }

    /// <summary>
    /// Called after the user gives permission to access the device gallery.
    /// </summary>
    private void GetImageHelper()
    {
        if (!gameManager.PermissionReceived)
        {
            // user did not give permission to view gallery
            DisplayDebug("User did not give permission to view gallery.");
            return;
        }

        if (gameManager.GetImageFromGallery(SetOriginalTexture))
        {
            DisplayDebug("Media Picker is busy. Try again.     ");
        }
    }

    private void SetOriginalTexture(Texture2D texture)
    {
        tempOriginalTexture = texture;
        tempOriginalSprite = Sprite.Create(tempOriginalTexture, new Rect(0.0f, 0.0f, tempOriginalTexture.width, tempOriginalTexture.height), new Vector2(0.5f, 0.5f));

        original.gameObject.SetActive(true);
        original.sprite = tempOriginalSprite;

        ResetPuzzleInfoDisplay();
    }

    /// <summary>
    /// Call this to initiate the process to get an image from device storage for custom puzzle creation.
    /// </summary>
    private void GetImageFromGallery()
    {
#if UNITY_EDITOR
        GetImageWhileInEditor();
        return;
#endif

#if UNITY_ANDROID
        if (gameManager.PermissionReceived)
        {
            GetImageHelper();
        }
        else
        {
            gameManager.RequestPermission();
            waitingForPermissionToGetImage = true;
        }
#endif

    }

    /// <summary>
    /// Set the current original sprite/texture from a stock image w/o using Android gallery.
    /// </summary>
    private void GetImageWhileInEditor()
    {
        tempOriginalTexture = testTexture;
        tempOriginalSprite = Sprite.Create(tempOriginalTexture, new Rect(0.0f, 0.0f, tempOriginalTexture.width, tempOriginalTexture.height), new Vector2(0.5f, 0.5f));

        original.gameObject.SetActive(true);
        original.sprite = tempOriginalSprite;

        makePuzzle.gameObject.SetActive(true);
        makePuzzle.interactable = true;
        ResetPuzzleInfoDisplay();
    }

    /// <summary>
    /// Reset the display to remove Puzzle info.
    /// </summary>
    private void ResetPuzzleInfoDisplay()
    {
        nameInput.text = "";
        colorsPanel.SetActive(false);

        tagInput.text = "";
        tagsPanel.SetActive(false);

        foreach (GameObject tag in tags)
        {
            tag.SetActive(false);
        }

        for (int i = 0; i < swatches.Length; i++)
        {
            swatches[i].gameObject.SetActive(false);
        }

        savePanel.SetActive(false);
        savePuzzle.gameObject.SetActive(false);
        pixelated.gameObject.SetActive(false);

        makePuzzle.gameObject.SetActive(true);
        makePuzzle.interactable = true;

        DisplayDebug("");
    }

    /// <summary>
    /// Call this after selecting an image from device storage to create a temporary puzzle from the image.
    /// </summary>
    private void CreatePuzzleFromImage()
    {
        ResetPuzzleInfoDisplay();
        if (tempOriginalTexture == null)
        {
            DisplayDebug("Need image file to create a custom puzzle.");
            GetImageFromGallery();
            return;
        }

        // Block user input while loading the puzzle
        Blocked = true;
        mainCanvas.gameObject.GetComponent<CanvasGroup>().interactable = false;
        DisplayDebug("Creating puzzle... ");
        // Make the puzzle!
        puzzleCreator.CreatePuzzle(tempOriginalSprite, 26);
    }

    /// <summary>
    /// Do these things after the puzzle is created!
    /// </summary>
    private void OnPuzzleCreated(int dimension)
    {
        // make the screen interactable
        Blocked = false;
        mainCanvas.gameObject.GetComponent<CanvasGroup>().interactable = true;
        // make a texture from the puzzle data
        tempPuzzleTexture = puzzleCreator.CurrentPuzzle.ReconstructTexture(dimension);
        // make a sprite from the texture
        tempPuzzleSprite = Sprite.Create(tempPuzzleTexture, new Rect(0.0f, 0.0f, tempPuzzleTexture.width, tempPuzzleTexture.height), new Vector2(0.5f, 0.5f));
        // display the puzzle sprite
        pixelated.gameObject.SetActive(true);
        pixelated.sprite = tempPuzzleSprite;

        // display the color swatches
        colorsPanel.SetActive(true);
        for (int i = 0; i < puzzleCreator.CurrentPuzzle.colors.Length; i++)
        {
            swatches[i].gameObject.SetActive(true);
            Vector3 vecColor = puzzleCreator.CurrentPuzzle.colors[i];
            Color c = new Color(vecColor.x, vecColor.y, vecColor.z);
            swatches[i].GetComponentsInChildren<Image>()[1].color = c;
        }
        // display the dimensions
        dimensions.text = $"Size: {puzzleCreator.CurrentPuzzle.wBoxes} x {puzzleCreator.CurrentPuzzle.hBoxes}";

        tagsPanel.SetActive(true);
        savePanel.SetActive(true);

        foreach (GameObject tag in tags)
        {
            tag.SetActive(false);
        }
        tags[0].SetActive(true);
        tags[0].GetComponentInChildren<Text>().text = "Custom";

        // show save button
        savePuzzle.gameObject.SetActive(true);
        savePuzzle.interactable = false;

        DisplayDebug($"Completed.  Number of colors: {puzzleCreator.CurrentPuzzle.colors.Length}  ");
        DisplayDebug($"Puzzle dimensions:  {puzzleCreator.CurrentPuzzle.wBoxes} x {puzzleCreator.CurrentPuzzle.hBoxes}  ");

        EventSystem.current.SetSelectedGameObject(null);
    }

    /// <summary>
    /// Call this to save the custom puzzle to the user's library of puzzles.
    /// </summary>
    private void SavePuzzle()
    {
        if (puzzleCreator.CurrentPuzzle == null)
        {
            DisplayDebug($"No puzzle to save.");
            return;
        }

        //! save tags
        List<string> tagStrings = new List<string>();
        foreach (GameObject tagObject in tags)
        {
            if (tagObject.activeInHierarchy)
            {
                tagStrings.Add(tagObject.GetComponentInChildren<Text>().text);
            }
        }

        puzzleCreator.CurrentPuzzle.Name = nameInput.textComponent.text;
        puzzleCreator.CurrentPuzzle.SetTags(tagStrings);

        gameManager.AddNewPuzzle(puzzleCreator.CurrentPuzzle);        

        // show popup screen w/ 3 options
        savePopup.enabled = true;
    }

    private void SaveCreationAndStay()
    {
        // hide popup screen
        savePopup.enabled = false;
        // reset puzzle creation screen
        ResetPuzzleInfoDisplay();
        original.gameObject.SetActive(false);
        makePuzzle.gameObject.SetActive(false);
    }

    public void CloseSavePopup()
    {
        savePopup.enabled = false;
    }
    
    //! TODO
    /// <summary>
    /// Click the puzzle image to view larger.
    /// </summary>
    private void ZoomPuzzleImage()
    {

    }

    /// <summary>
    /// Add a tag to the Puzzle, and display it in the panel.
    /// </summary>
    public void AddTag()
    {
        string tag = tagInput.textComponent.text;

        if (tag == null) return;
        if (tag == "") return;

        foreach (GameObject tagObject in tags)
        {
            // check for tag already added
            if (tagObject.activeInHierarchy)
                if (tagObject.GetComponentInChildren<Text>().text.CompareTo(tag) == 0) return;
        }
        foreach (GameObject tagObject in tags)
        {
            // find an open tag
            if (!tagObject.activeInHierarchy)
            {
                tagObject.SetActive(true);
                tagObject.GetComponentInChildren<Text>().text = tag;
                tagObject.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                tagObject.GetComponentInChildren<Button>().onClick.AddListener(() => RemoveTag(tag));
                tagInput.text = "";
                return;
            }
        }
        return;
    }

    /// <summary>
    /// Remove a tag from the puzzle, and remove its display.
    /// </summary>
    /// <param name="s"></param>
    public void RemoveTag(string s)
    {
        if (s == null) return;
        if (s == "") return;
        if (s.CompareTo("Custom") == 0) return;

        foreach (GameObject tagObject in tags)
        {
            if (tagObject.activeInHierarchy)
                if (tagObject.GetComponentInChildren<Text>().text.CompareTo(s) == 0)
                {
                    tagObject.GetComponentInChildren<Text>().text = "";
                    tagObject.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                    tagObject.SetActive(false);
                    return;
                }
        }
        return;
    }

    // Text field to display console lines on the screen
    public Text debugTextDisplay;

    /// <summary>
    /// Used to display messages to the debugging console or text displayed on screen.
    /// </summary>
    /// <param name="s"></param>
    public override void DisplayDebug(string s)
    {
        Debug.Log(s);
        debugTextDisplay.text += s;
    }
}

public delegate void TextureDelegate(Texture2D texture);
