﻿// K-Means Clustering algorithm modified from James McCaffrey's KMeansDemo.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;

public class PuzzleCreation : MonoBehaviour
{
    public Puzzle CurrentPuzzle { get; private set; }

    public event Action<int> PuzzleCreated;

    #region Public Functions

    public void CreatePuzzle(Sprite sourceSprite, int numColors)
    {
        int num = numColors;
        int boxWidthInPixels = Mathf.CeilToInt(sourceSprite.texture.width / (float)GameManager.MAX_WBOXES);
        int boxHeightInPixels = Mathf.CeilToInt(sourceSprite.texture.height / (float)GameManager.MAX_WBOXES);
        int boxDimension = Mathf.Max(boxWidthInPixels, boxHeightInPixels);

        if (numColors > GameManager.MAX_COLORS)
        {
            Debug.LogError($"Too many colors. Max is {GameManager.MAX_COLORS}");
            num = GameManager.MAX_COLORS;
        }

        PixelatedInfo pixelated = Pixelate(sourceSprite.texture.DuplicateTexture(), boxDimension);
                
        ClusteringCompleted = null;
        ClusteringCompleted += () => {
            CurrentPuzzle = new Puzzle(pixelated.columns, pixelated.rows, myClusterIds, myClusterValues.ToSerializable());
            PuzzleCreated?.Invoke(boxDimension);
        };

        KMeansClustering(pixelated.colors, num);        
    }
    
    #endregion

    #region Image Manipulation Private Functions
   
    private Vector3[] myData;
    private int myNumClusters;
    private int[] myClusterIds;
    private Vector3[] myClusterValues;
    private IEnumerator coroutine;
    private event Action ClusteringCompleted;
    public event Action<int> Looped;
    private PixelatedInfo Pixelate(Texture2D sourceTexture, int boxDimensions)
    {
        if (!sourceTexture || boxDimensions < 2)
        {
            Debug.LogError("Invalid parameters to Pixelate.");
            return null;
        }
        
        Texture2D resultTexture = new Texture2D(sourceTexture.width - (sourceTexture.width % boxDimensions), sourceTexture.height - (sourceTexture.height % boxDimensions));
        resultTexture.filterMode = FilterMode.Point;

        Color[] sourceColors = sourceTexture.GetPixels();

        int wBoxes, hBoxes, wPixels, hPixels;
        wPixels = sourceTexture.width;
        hPixels = sourceTexture.height;
        wBoxes = wPixels / boxDimensions; // discards extra pixels on the right!
        hBoxes = hPixels / boxDimensions; // discards extra pixels on the top!

        Vector3[] averageColors = new Vector3[wBoxes * hBoxes];

        for (int h = 0; h < hBoxes; h++)
        {
            for (int w = 0; w < wBoxes; w++)
            {
                int bottomLeftPixelIndex = (wPixels * boxDimensions * h) + (boxDimensions * w);
                float r = 0, g = 0, b = 0;

                // get the average color of the pixels in the box from the source image
                for (int i = 0; i < boxDimensions; i++)
                {
                    for (int j = 0; j < boxDimensions; j++)
                    {
                        Color c = sourceColors[bottomLeftPixelIndex + i + (j * wPixels)];

                        r += c.r;
                        g += c.g;
                        b += c.b;
                    }
                }

                r /= (boxDimensions * boxDimensions);
                g /= (boxDimensions * boxDimensions);
                b /= (boxDimensions * boxDimensions);

                // save the averaged color to the array of colors
                int averageColorsIndex = w + (h * wBoxes);
                averageColors[averageColorsIndex] = new Vector3(r, g, b);
                
                //set all the pixels in the result image to the average color
                for (int k = 0; k < boxDimensions; k++)
                {
                    for (int l = 0; l < boxDimensions; l++)
                    {
                        int x = w * boxDimensions + k;
                        int y = h * boxDimensions + l;
                        resultTexture.SetPixel(x, y, new Color(r, g, b));
                    }
                }
            }
        }

        resultTexture.Apply();

        Debug.Log($"Puzzle data: {wBoxes} boxes wide");

        return new PixelatedInfo(
            Sprite.Create(resultTexture, new Rect(0, 0, resultTexture.width, resultTexture.height), new Vector2(0.5f, 0.5f), 100),
            averageColors, wBoxes, hBoxes);
    }
    
    // Assumes normalized data (color vectors' values are >= 0 and <= 1)
    private void KMeansClustering(Vector3[] data, int numClusters)
    {
        if (data?.Length < 2 || numClusters < 2 || data?.Length < numClusters)
            return;

        myData = new Vector3[data.Length];
        Array.Copy(data, myData, data.Length);

        //Debug.Log($"Start clustering. {Time.time}   ");

        myNumClusters = numClusters;

        myClusterIds = new int[data.Length];
        myClusterValues = new Vector3[myNumClusters];

        /// set random cluster ids, making sure at least one in each cluster
        for (int i = 0; i < myNumClusters; ++i)
            myClusterIds[i] = i;
        for (int i = myNumClusters; i < myClusterIds.Length; ++i)
            myClusterIds[i] = UnityEngine.Random.Range(0, myNumClusters);
        for (int i = 0; i < myNumClusters; i++)
            myClusterValues[i] = new Vector3();

        #region old stuff
        //Debug.Log($"Clustering intialized. {Time.time}   ");

        //bool changed = true; // was there a change in at least one cluster assignment?
        //bool success = true; // were all means able to be computed? (no zero-count clusters)

        ///// loop
        //int maxCount = data.Length * 10;
        //int ct = 0;
        //while (changed == true && success == true && ct < maxCount)
        //{
        //    ++ct;
        //    /// compute mean of each cluster
        //    Tuple<bool, Vector3[]> updatedMeans = UpdateMeans(data, clusterIds, num);
        //    success = updatedMeans.Item1;
        //    if (success)
        //        Array.Copy(updatedMeans.Item2, means, updatedMeans.Item2.Length);

        //    /// update clustering based on new means
        //    Tuple<bool, int, int[], Vector3[]> updatedClusters = UpdateClustering(data, clusterIds, means);
        //    changed = updatedClusters.Item1;
        //    num = updatedClusters.Item2;
        //    if (changed)
        //    {
        //        Array.Copy(updatedClusters.Item3, clusterIds, updatedClusters.Item3.Length);
        //        Array.Copy(updatedClusters.Item4, means, updatedClusters.Item4.Length);
        //    }
        //    //Debug.Log($"Cluster loop: {ct} {Time.time}   ");
        //}
        ///// end loop
        #endregion

        coroutine = Cluster();

        StartCoroutine(coroutine);        
    }

    IEnumerator Cluster()
    {
        bool changed = true; // was there a change in at least one cluster assignment?
        bool success = true; // were all means able to be computed? (no zero-count clusters)
        int maxCount = myData.Length * 10;
        int ct = 0;
        while (changed == true && success == true && ct < maxCount)
        {
            ++ct;
            Looped?.Invoke(ct);
            /// compute mean of each cluster
            Tuple<bool, Vector3[]> updatedMeans = UpdateMeans(myData, myClusterIds, myNumClusters);
            success = updatedMeans.Item1;
            if (success)
                Array.Copy(updatedMeans.Item2, myClusterValues, updatedMeans.Item2.Length);

            /// update clustering based on new means
            Tuple<bool, int, int[], Vector3[]> updatedClusters = UpdateClustering(myData, myClusterIds, myClusterValues);
            changed = updatedClusters.Item1;
            myNumClusters = updatedClusters.Item2;
            if (changed)
            {
                Array.Copy(updatedClusters.Item3, myClusterIds, updatedClusters.Item3.Length);
                Array.Copy(updatedClusters.Item4, myClusterValues, updatedClusters.Item4.Length);
            }
            yield return null;
        }
        ClusteringCompleted?.Invoke();
    }
        
    private Tuple<bool, Vector3[]> UpdateMeans(Vector3[] data, int[] clusterIds, int numClusters)
    {
        //Debug.Log($"Updating means. {Time.time}    ");

        Vector3[] means = new Vector3[numClusters];
        for (int k = 0; k < means.Length; ++k)
            means[k] = Vector3.zero;

        // count the number of data points in each cluster
        int[] clusterCounts = new int[numClusters];
        for (int i = 0; i < data.Length; ++i)
        {
            int cluster = clusterIds[i];
            ++clusterCounts[cluster];
        }

        // check if any clusters have 0 data points
        for (int k = 0; k < numClusters; ++k)
            if (clusterCounts[k] == 0)
                return new Tuple<bool, Vector3[]>(false, null);

        // get the mean value of all the data points in each cluster
        for (int i = 0; i < data.Length; ++i)
        {
            int cluster = clusterIds[i];
            means[cluster] += data[i];
        }
        for (int k = 0; k < means.Length; ++k)
        {
            means[k] /= clusterCounts[k];
        }

        return new Tuple<bool, Vector3[]>(true, means);
    }

    /// <summary>
    /// Returns a Tuple with values:
    /// Item1: bool - true if the clustering was successful.
    /// Item2: int - number of clusters after this round of clustering.
    /// Item3: int[] - cluster ids of each data point.
    /// Item4: Vector3[] - cluster values, indexed by cluster id.
    /// </summary>
    /// <param name="dataPoints"></param>
    /// <param name="clusterIds"></param>
    /// <param name="clusterCenters"></param>
    /// <returns></returns>
    private Tuple<bool, int, int[], Vector3[]> UpdateClustering(Vector3[] dataPoints, int[] clusterIds, Vector3[] clusterCenters)
    {
        //Debug.Log($"Updating clustering. {Time.time}   ");

        int numClusters = clusterCenters.Length;
        bool changed = false;

        int[] newClusterIds = new int[clusterIds.Length];
        Array.Copy(clusterIds, newClusterIds, clusterIds.Length);

        double[] distances = new double[numClusters];

        // for each data point
        for (int i = 0; i < dataPoints.Length; ++i)
        {
            // get the distance between the data point and all the cluster centers
            for (int k = 0; k < numClusters; ++k)
                distances[k] = Vector3.Distance(dataPoints[i], clusterCenters[k]);

            // find the closest cluster center
            int newClusterID = MinIndex(distances);

            // set the new cluster id
            if (newClusterID != newClusterIds[i])
            {
                changed = true;
                newClusterIds[i] = newClusterID;
            }
        }

        // check if no cluster ids were changed
        if (changed == false)
            return new Tuple<bool, int, int[], Vector3[]>(false, numClusters, null, null);

        // find how many data points in each cluster
        int[] clusterCounts = new int[numClusters];
        for (int i = 0; i < dataPoints.Length; ++i)
        {
            int cluster = newClusterIds[i];
            ++clusterCounts[cluster];
        }

        // find all the cluster ids with at least one data point
        List<int> goodClusterIds = new List<int>();
        for (int k = 0; k < numClusters; ++k)
            if (clusterCounts[k] != 0)
            {
                goodClusterIds.Add(k);
            }
        
        // update the clusters to remove empty clusters
        if (goodClusterIds.Count < numClusters)
        {
            // update the ids
            for (int i = 0; i < newClusterIds.Length; i++)
            {
                newClusterIds[i] = goodClusterIds.IndexOf(newClusterIds[i]);
            }
            // update the cluster values
            Vector3[] newClusterCenters = new Vector3[goodClusterIds.Count];
            for (int i = 0; i < goodClusterIds.Count; i++)
            {
                newClusterCenters[i] = clusterCenters[goodClusterIds[i]];
            }
            return new Tuple<bool, int, int[], Vector3[]>(true, goodClusterIds.Count, newClusterIds, newClusterCenters);
        }
        return new Tuple<bool, int, int[], Vector3[]>(true, numClusters, newClusterIds, clusterCenters);
    }

    private int MinIndex(double[] distances)
    {
        // index of smallest value in array
        int indexOfMin = 0;
        double smallDist = distances[0];
        for (int k = 0; k < distances.Length; ++k)
        {
            if (distances[k] < smallDist)
            {
                smallDist = distances[k];
                indexOfMin = k;
            }
        }
        return indexOfMin;
    }

    #endregion

    private class PixelatedInfo
    {
        public Sprite sprite;
        public Vector3[] colors;    // averaged colors, indexed by w + (h * wBoxes)
        public int columns, rows;   // number of columns of boxes (wBoxes) and rows of boxes (hBoxes)

        public PixelatedInfo(Sprite s, Vector3[] v, int c, int r)
        {
            sprite = s;
            colors = v;
            columns = c;
            rows = r;
        }
    }
}