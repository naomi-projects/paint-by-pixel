﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LibraryItem : MonoBehaviour, IPointerClickHandler
{
    public Image puzzleImage;
    public Text puzzleName;
    public Text dimensions;
    public Image button;

    public event Action Selected;

    public IndexedPuzzle IndexedPuzzle { get; private set; }
    
    public void SetPuzzle(Puzzle p, int index)
    {
        IndexedPuzzle = new IndexedPuzzle(p, index);
        Texture2D tex = p.ReconstructTexture();
        puzzleImage.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        puzzleName.text = p.Name;
        dimensions.text = $"{p.wBoxes} x {p.hBoxes}";
    }

    public void Deselect()
    {
        // reset button image to default color
        Color c = button.color;
        c.a = 0f;
        button.color = c;
    }

    public void Select()
    {
        // set highlight on button image
        Color c = button.color;
        c.a = 0.25f;
        button.color = c;

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log($"{this.gameObject.name} clicked.");
        Selected?.Invoke();
        Select();
    }
}
