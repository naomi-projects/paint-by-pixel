﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserInterfaceManager : MonoBehaviour
{
    // Screens that make up the app interface
    public Canvas loadingScreen, titleScreen;

    private PuzzleView puzzleView;
    private PuzzleCreationView puzzleCreationView;
    private LibraryView libraryView;

    private GameManager gameMgr;
           
    /// <summary>
    /// Start function - Setup
    /// </summary>
    private void Start()
    {
        gameMgr = GameManager.Instance;
        puzzleView = FindObjectOfType<PuzzleView>();
        puzzleCreationView = FindObjectOfType<PuzzleCreationView>();
        libraryView = FindObjectOfType<LibraryView>();

        if (puzzleView == null) Debug.LogError("PuzzleView does not exist in the scene.");
        if (puzzleCreationView == null) Debug.LogError("PuzzleCreationView does not exist in the scene.");
        if (libraryView == null) Debug.LogError("LibraryView does not exist in the scene.");
        if (!gameMgr) Debug.LogError("GameManager does not exist in the scene.");

        gameMgr.ChangedGameState += (prev, state) => {
            // show/hide screens when changing states (states represent UI screens)
            loadingScreen.gameObject.SetActive(state == GameState.Loading);
            titleScreen.gameObject.SetActive(state == GameState.TitleScreen);

            if (prev == GameState.InPuzzle)
            {
                puzzleView.Exit();
            }

            puzzleCreationView.Activate(state == GameState.PuzzleCreation);
            libraryView.Activate(state == GameState.Library);
            puzzleView.Activate(state == GameState.InPuzzle);
        };

        gameMgr.SetGameState(GameState.TitleScreen);

    }    

    // Used to block user input (mainly pressing Escape) while a puzzle is being created
    public bool Blocked { get; private set; }

    /// <summary>
    /// Update() called every frame.
    /// </summary>
    private void Update()
    {

        if (Blocked) return;        

        // User presses Back button
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (gameMgr.CurrentGameState)
            {
                case GameState.Loading:
                    break;
                case GameState.TitleScreen:
				// change gamestate to quit
                    QuitApplication();
                    break;
                case GameState.Library:
				// change gamestate to title
                    GoToTitleScreen();
                    break;
                case GameState.PuzzleCreation:
				// change gamestate to library
                    GoToLibrary();
                    break;
                case GameState.InPuzzle:
				// change gamestate to library
                    GoToLibrary();
                    break;
                default:
                    break;
            }
        }

        // Checking which keys are pressed. Mouse0 is one-finger touch. Mouse1 is 2-finger touch.
        foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(vKey))
            {
                //Debug.Log($"Key pressed: {vKey}");
            }
        }        		
    }
    
    #region Button Functions to Navigate Screens

    public void GoToTitleScreen() => gameMgr.SetGameState(GameState.TitleScreen);
    public void GoToLibrary() => gameMgr.SetGameState(GameState.Library);
    public void GoToPuzzleCreator() => gameMgr.SetGameState(GameState.PuzzleCreation);
    public void GoToPuzzle()
    {
        if (gameMgr.CurrentGameState == GameState.Library)
        {
            if (libraryView.SelectedLibraryItem == null) return;
            gameMgr.SetMostRecent(libraryView.SelectedLibraryItem.IndexedPuzzle.IndexInSaveData);
        }
        else if (gameMgr.CurrentGameState == GameState.PuzzleCreation)
        {
            gameMgr.SetMostRecent(gameMgr.CurrentSave.Puzzles.Count - 1);
        }
        else if (gameMgr.CurrentGameState == GameState.TitleScreen)
        {
            // if loading, show the loading screen
            if (!puzzleView.IsLoaded)
            {
                puzzleView.DoneLoading += () => gameMgr.SetGameState(GameState.InPuzzle); 
                gameMgr.SetGameState(GameState.Loading);
                return;
            }
            // when done loading, hide the loading screen
        }
        gameMgr.SetGameState(GameState.InPuzzle);
    }

    public void QuitApplication() // => GameManager.Instance.SetGameState(GameState.Quiting);
    {
        // do stuff, save stuff
        Application.Quit();
    }
    #endregion
}

//! 
public class IndexedPuzzle
{
    public Puzzle Puzzle { get; private set; }
    public int IndexInSaveData { get; private set; }

    public IndexedPuzzle(Puzzle p, int index)
    {
        Puzzle = p;
        IndexInSaveData = index;
    }
}



