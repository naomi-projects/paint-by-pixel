﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Puzzle
{
    #region Public Variables

    // Puzzle Data
    public int wBoxes;         // puzzle width in boxes
    public int hBoxes;         // puzzle height in boxes
    public int[] colorIds;     // color id of each box in the puzzle, from left to right and bottom to top
    public SerializableVector3[] colors;   // array of unique colors used in the puzzle, indexes are color ids
    public string Name { get; set; }        // name of the puzzle
    public List<string> puzzleTags;

    // Puzzle Progress
    public int[] currentColors; // color ids of currently filled colors, -1 if not colored   

    // true if this puzzle has ever been completed (not if it is currently completed)
    public bool HasBeenCompleted { get; private set; }
    
    #endregion


    /// <summary>
    /// Create a new Puzzle.
    /// </summary>
    /// <param name="w">Puzzle width in boxes.</param>
    /// <param name="h">Puzzle height in boxes.</param>
    /// <param name="ids">Color ID of each box.</param>
    /// <param name="values">Color value of each color ID.</param>
    /// <param name="name">Name of the Puzzle.</param>
    public Puzzle(int w, int h, int[] ids, SerializableVector3[] values, string name = "My Puzzle")
    {
        wBoxes = w;
        hBoxes = h;
        colorIds = new int[ids.Length];
        Array.Copy(ids, colorIds, ids.Length);
        colors = new SerializableVector3[values.Length];
        Array.Copy(values, colors, values.Length);
        Name = name;

        currentColors = new int[ids.Length];
		//! set to 0, add 1 to color ids
        for (int i = 0; i < currentColors.Length; i++) { currentColors[i] = -1; }
    }

    // only used for one-time initialization
    public void SetTags(List<string> tagStrings)
    {
        if (tagStrings != null && puzzleTags == null)
        {
            if (tagStrings.Count > 0 && tagStrings.Count < 7)
            {
                puzzleTags = new List<string>();
                foreach (string tag in tagStrings)
                {
                    puzzleTags.Add(tag);
                }
            }
        }
    }

    /// <summary>
    /// Reconstruct a Texture2D from the puzzle info.
    /// </summary>
    /// <param name="boxDimensions"></param>
    /// <returns></returns>
    public Texture2D ReconstructTexture(int boxDimensions = 1)
    {
        Texture2D resultTexture = new Texture2D(wBoxes * boxDimensions, hBoxes * boxDimensions);
        resultTexture.filterMode = FilterMode.Point;

        int wPixels = boxDimensions * wBoxes;

        // for each box
        for (int h = 0; h < hBoxes; h++)
        {
            for (int w = 0; w < wBoxes; w++)
            {
                SerializableVector3 vecColor = colors[colorIds[w + (h * wBoxes)]];
                Color c = new Color(vecColor.x, vecColor.y, vecColor.z);

                // set all the pixels in the box to the color
                for (int k = 0; k < boxDimensions; k++)
                {
                    for (int l = 0; l < boxDimensions; l++)
                    {
                        int x = w * boxDimensions + k;
                        int y = h * boxDimensions + l;
                        resultTexture.SetPixel(x, y, c);
                    }
                }
            }
        }
        resultTexture.Apply();
        return resultTexture;
    }

    /// <summary>
    /// Get the number of correctly filled pixels in the puzzle.
    /// </summary>
    public int NumberOfCorrectColors()
    {
        int correct = 0;
        for (int i = 0; i < currentColors.Length; i++)
        {
            if (currentColors[i] == colorIds[i]) correct++;
        }
        return correct;
    }

    /// <summary>
    /// Get the percentage completion of the puzzle.
    /// </summary>
    public float CurrentCompletion() => NumberOfCorrectColors() / (float)colorIds.Length;

    /// <summary>
    /// Compares Puzzles using their names. This assumes unique naming!
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public bool Equals(Puzzle p) => this.Name.CompareTo(p.Name) == 0;
}
