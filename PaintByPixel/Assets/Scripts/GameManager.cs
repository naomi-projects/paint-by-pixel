﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    public SaveState CurrentSave { get; private set; }

    public bool PermissionReceived => Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead);
    public void RequestPermission()
    {
        Permission.RequestUserPermission(Permission.ExternalStorageRead);
    }

    public const int MAX_COLORS = 32;
    public const int MAX_WBOXES = 200;

    public GameState CurrentGameState { get; private set;  }

    public event Action<GameState, GameState> ChangedGameState; 

    public bool SetGameState(GameState newState)
    {
        if (newState == CurrentGameState) return false;
        GameState prev = CurrentGameState;
        CurrentGameState = newState;
        //Debug.Log($"Current game state: {CurrentGameState.ToString()}");
        ChangedGameState?.Invoke(prev, CurrentGameState);
        return true;
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);

		// Load from save file
        CurrentSave = SaveSystem.Load();
        if (CurrentSave == null)
        {
            Debug.Log($"Created new save.");
            CurrentSave = new SaveState();
        }
    }
    
    public void AddNewPuzzle(Puzzle p)
    {
        CurrentSave.AddNewPuzzle(p);
        SaveSystem.Save(CurrentSave);
    }

    public void SetMostRecent(int index)
    {
        if (CurrentSave.SetMostRecent(index))
            SaveSystem.Save(CurrentSave);
    }

    /// <summary>
    /// Access device storage to get an image. Calls MyFunc if an image is picked.
    /// </summary>
    /// <param name="MyFunc">Called on the Texture2D picked.</param>
    /// <returns></returns>
    public bool GetImageFromGallery(TextureDelegate MyFunc)
    {
        if (NativeGallery.IsMediaPickerBusy())
        {
            return false;
        }
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) => {         
            
            if (path != null)
            {
                MyFunc(NativeGallery.LoadImageAtPath(path));
            }

        }, "Select an image");
        return true;
    }
}

public enum GameState { Loading, TitleScreen, Library, PuzzleCreation, InPuzzle, Quiting }

