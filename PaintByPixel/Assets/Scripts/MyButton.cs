﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class MyButton : MonoBehaviour, IPointerDownHandler
{
    public Image[] highlights;
    public TextMeshProUGUI letterId;

    public int myInt { get; set; }

    public event Action<int> Selected;

    protected bool isSelected;

    public virtual void Select(bool select)
    {
        if (isSelected == select) return;
        isSelected = select;

        // set the highlight on
        foreach (Image highlight in highlights)
        {
            highlight.gameObject.SetActive(select);
        }
        // set the text color: black
        letterId.color = select ? Color.black : Color.white;
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        Select(true);
        Selected?.Invoke(myInt);
    }
}
