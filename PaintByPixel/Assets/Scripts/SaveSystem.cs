﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;

public static class SaveSystem 
{
    public static string saveFileName = "/PBP.save";

    public static void Save(SaveState state)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + saveFileName;
        FileStream stream = new FileStream(path, FileMode.Create);

        Debug.Log($"Saving data to {path}");
        formatter.Serialize(stream, state);
        stream.Close();
    }

    public static SaveState Load()
    {
        string path = Application.persistentDataPath + saveFileName;
        if (File.Exists(path))
        {
            Debug.Log($"Save data found at {path}");
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveState state = formatter.Deserialize(stream) as SaveState;

            stream.Close();

            return state;
        }
        else
        {
            Debug.LogWarning($"Save file not found in {path}");
            return null;
        }
    }

}

[System.Serializable]
public class SaveState
{
    public int IndexOfMostRecent { get; private set; }

    [SerializeField]
    public List<Puzzle> Puzzles { get; private set; }

    [SerializeField]
    public List<string> PuzzleTags { get; private set; }

    /// <summary>
    /// Create a new empty save state.
    /// </summary>
    public SaveState()
    {
        Puzzles = new List<Puzzle>();
        PuzzleTags = new List<string>();
    }

    /// <summary>
    /// Add a puzzle to the list of puzzles.
    /// </summary>
    public void AddNewPuzzle(Puzzle p)
    {
        // unique non-blank names!
        if (p.Name == null) p.Name = "My Puzzle";
        else if (p.Name.CompareTo("") == 0) p.Name = "My Puzzle";

        if (Puzzles.Exists(puzzle => puzzle.Name.CompareTo(p.Name) == 0)) p.Name += " (1)";
        Puzzles.Add(p);
        Debug.Log("Puzzle added to save.");

        // check for new tags
        foreach (string tag in p.puzzleTags)
        {
            if (!PuzzleTags.Contains(tag)) PuzzleTags.Add(tag);
        }
    }

    /// <summary>
    /// Set the index number of the most recently worked on puzzle. 
    /// Returns false if nothing was changed.
    /// </summary>
    /// <param name="index"></param>
    /// <returns>Returns false if nothing was changed.</returns>
    public bool SetMostRecent(int index)
    {
        if (index > Puzzles.Count || index < 0)
        {
            Debug.LogError($"Index of puzzle out of bounds: {index}");
            return false;
        }
        if (IndexOfMostRecent == index) return false;

        IndexOfMostRecent = index;
        return true;
    }

    /// <summary>
    /// Get the most recently worked on puzzle
    /// </summary>
    public Puzzle GetMostRecent() => Puzzles[IndexOfMostRecent];

    /// <summary>
    /// Get the puzzle at a specified index.
    /// </summary>
    public Puzzle GetPuzzle(int index)
    {
        if (index > Puzzles.Count || index < 0)
        {
            Debug.LogError($"Index of puzzle out of bounds: {index}");
            return null;
        }
        return Puzzles[index];
    }

    /// <summary>
    /// Get the first listed puzzle with the specified name.
    /// </summary>
    public Puzzle GetPuzzle(string name) => Puzzles.Where(puzzle => puzzle.Name.CompareTo(name) == 0).First();

    /// <summary>
    /// Get all the puzzles with a specific puzzle tag.
    /// </summary>
    public List<Puzzle> GetPuzzles(string tag) => Puzzles.Where(p => p.puzzleTags.Contains(tag)).ToList();


    /// <summary>
    /// Get all the complete or incomplete puzzles.
    /// </summary>
    public List<Puzzle> GetPuzzles(bool completed) => Puzzles.Where(p => p.HasBeenCompleted == completed).ToList();

    /// <summary>
    /// Get the list of puzzles ordered by number of pixels.
    /// </summary>
    public List<Puzzle> GetPuzzlesBySize() => Puzzles.OrderBy(p => p.wBoxes * p.hBoxes).ToList();

    /// <summary>
    /// Get the list of puzzles ordered by name.
    /// </summary>
    public List<Puzzle> GetPuzzlesByName() => Puzzles.OrderBy(p => p.Name).ToList();

    /// <summary>
    /// Get all the puzzles.
    /// </summary>
    public List<Puzzle> GetPuzzles() => Puzzles;

    /// <summary>
    /// Get the total number of puzzles.
    /// </summary>
    public int NumberOfPuzzles() => Puzzles.Count;

    /// <summary>
    /// Get the number of completed or incompleted puzzles.
    /// </summary>
    public int NumberOfCompleted(bool completed) => Puzzles.Where(p => p.HasBeenCompleted == completed).Count();
}
