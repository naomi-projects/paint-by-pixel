﻿using System.Collections;
using UnityEngine;

public static class ExtensionMethods
{
    public static string ToColorId(this int num)
    {
        if (num > 25)
            return (num - 25).ToString();
        else
            return ((char)(num + 65)).ToString();
    }

    public static SerializableVector3 ToSerializable(this Vector3 vec)
    {
        return new SerializableVector3(vec);
    }

    public static SerializableVector3[] ToSerializable(this Vector3[] vecs)
    {
        SerializableVector3[] serVecs = new SerializableVector3[vecs.Length];
        for (int i = 0; i < vecs.Length; i++)
        {
            serVecs[i] = vecs[i].ToSerializable();
        }
        return serVecs;
    }

    public static Texture2D DuplicateTexture(this Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                source.width,
                source.height,
                0,
                RenderTextureFormat.Default,
                RenderTextureReadWrite.Linear);
        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }
}

[System.Serializable]
public struct SerializableVector3
{
    public float x;
    public float y;
    public float z;

    public SerializableVector3(Vector3 vec)
    {
        x = vec.x;
        y = vec.y;
        z = vec.z;
    }

    public static implicit operator Vector3(SerializableVector3 serVec)
    {
        return new Vector3(serVec.x, serVec.y, serVec.z);
    }

    public static implicit operator Color(SerializableVector3 serVec)
    {
        return new Color(serVec.x, serVec.y, serVec.z);
    }
}