﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class ColorSwatch : MyButton
{
    public Image colorSwatch;
    public void SetColor(Vector3 vec)
    {
        colorSwatch.color = new Color(vec.x, vec.y, vec.z);
    }
}
