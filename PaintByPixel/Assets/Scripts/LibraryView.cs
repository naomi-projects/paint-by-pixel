﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LibraryView : View
{
    /// <summary>
    /// Reference to the GameManager in the scene.
    /// </summary>
    private GameManager gameManager;

    public Canvas mainCanvas;
    public GameObject libraryPanel;
    public GameObject libraryPuzzleDisplayPrefab;
    public Button startFromLibrary;
    public GameObject libraryTagsScrollView;
    public GameObject libraryTagPrefab;
    public Button showPremade, showCustom;

    public LibraryItem SelectedLibraryItem { get; private set; }

    private void Start()
    {
        gameManager = GameManager.Instance;
        if (!gameManager) Debug.LogError("GameManager does not exist in the scene.");

        SetupLibraryButtons();
    }
    
    public override void Activate(bool active)
    {
        mainCanvas.enabled = active;
        if (!active) return;

        ReloadLibraryDisplay();
    }

    protected override void UpdateFunction()
    {

        // Library Screen: start button
        if (SelectedLibraryItem == null) startFromLibrary.interactable = false;
        else startFromLibrary.interactable = true;
    }

    /// <summary>
    /// Refresh the display of Puzzles on re-entering the Library screen.
    /// </summary>
    private void ReloadLibraryDisplay(string tag = null, bool exclude = false)
    {
        SelectedLibraryItem = null;

        foreach (LibraryItem item in libraryPanel.GetComponentsInChildren<LibraryItem>())
        {
            Destroy(item.gameObject);
        }

        foreach (Button item in libraryTagsScrollView.GetComponentsInChildren<Button>())
        {
            Destroy(item.gameObject);
        }

        for (int i = 0; i < gameManager.CurrentSave.PuzzleTags.Count; i++)
        {
            GameObject tagButton = Instantiate(libraryTagPrefab, libraryTagsScrollView.transform);
            string s = gameManager.CurrentSave.PuzzleTags[i];
            tagButton.GetComponent<Text>().text = s;
            tagButton.GetComponent<Button>().onClick.AddListener(() => {
                ReloadLibraryDisplay(s);
            });
        }

        List<Puzzle> puzzlesList = gameManager.CurrentSave.GetPuzzles();

        for (int i = 0; i < puzzlesList.Count; i++)
        {
            if (tag != null)
            {
                if ((puzzlesList[i].puzzleTags.Contains(tag) && exclude)
                    || (!puzzlesList[i].puzzleTags.Contains(tag) && !exclude))
                {
                    continue;
                }
            }

            LibraryItem item = Instantiate(libraryPuzzleDisplayPrefab, libraryPanel.transform)
                .GetComponent<LibraryItem>();
            item.SetPuzzle(puzzlesList[i], i);
            item.Selected += () =>
            {
                if (SelectedLibraryItem != null) SelectedLibraryItem.Deselect();
                SelectedLibraryItem = item;
            };
        }
    }

    private void SetupLibraryButtons()
    {
        showPremade.onClick.RemoveAllListeners();
        showCustom.onClick.RemoveAllListeners();

        showPremade.onClick.AddListener(() => {
            ReloadLibraryDisplay("Custom", true);
        });
        showCustom.onClick.AddListener(() => {
            ReloadLibraryDisplay("Custom");
        });
    }
}
