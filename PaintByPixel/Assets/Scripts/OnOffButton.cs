﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class OnOffButton : MyButton
{
    public event Action<bool> Toggle;
    public override void OnPointerDown(PointerEventData eventData)
    {
        bool b = isSelected;
        Select(!b);
        Toggle?.Invoke(!b);
    }

    public void Click()
    {
        bool b = isSelected;
        Select(!b);
        Toggle?.Invoke(!b);
    }
}
