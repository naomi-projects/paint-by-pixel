﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class View : MonoBehaviour
{
    /// <summary>
    /// Use to activate/reset the display for this View.
    /// </summary>
    public abstract void Activate(bool active);

    // Used to block user input (mainly pressing Escape) while a puzzle is being created
    public bool Blocked { get; protected set; }
    protected abstract void UpdateFunction();

    private void Update()
    {
        if (Blocked) return;

        UpdateFunction();
    }

    /// <summary>
    /// Used to display messages to the debugging console or text displayed on screen.
    /// </summary>
    /// <param name="s"></param>
    public virtual void DisplayDebug(string s)
    {
        Debug.Log(s);
    }

}
